defmodule HashWorker do
    def starthashactor(numberzeroes, randlength, masterserver, mode) do
        randstring = Base.encode64 (:crypto.strong_rand_bytes(randlength))
        uppernonce = 10 * (:math.pow(10, numberzeroes))
        loopstrings(randstring, uppernonce, 0, numberzeroes, masterserver, mode)
        :done
      end
    
      def loopstrings(randstring, uppernonce, currentnonce, numberzeroes, masterserver, mode) do
        teststring = "kraghunathan" <> randstring <> to_string(currentnonce)
        case hashandtest(teststring, numberzeroes) do
          nil -> nil
          hashedstring when mode == :local -> send(masterserver, {teststring, hashedstring})
          hashedstring when mode == :remote ->  Node.spawn(masterserver, fn -> send(:listenerpid, {teststring, hashedstring}) end)
        end
        if currentnonce < uppernonce do
          loopstrings(randstring, uppernonce, currentnonce + 1, numberzeroes, masterserver, mode)
        end
      end
    
      def hashandtest(string, numberzeroes) do
        hashedstring = Base.encode16(:crypto.hash(:sha256, string))
        if testforleadingzeros(hashedstring, numberzeroes) do
          hashedstring
        else
          nil
        end
      end
    
      def testforleadingzeros(hashedstring, numberzeroes) do
        if numberzeroes == 0 do
          true
        else
          case hashedstring do
          <<"0", restofstring :: binary>> -> testforleadingzeros(restofstring, numberzeroes - 1)
          _ -> false
          end
        end
      end
    
end