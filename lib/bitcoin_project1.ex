defmodule BitcoinProject1 do
  @moduledoc """
  Overall supervisor module for each Node. Contains the entry point, spawns off multiple workers
  """

  @doc """
  Entry point into the project. Only tests for argument length
  """
  def main(opts) do
    if(length(opts) != 1) do
      raise "Incorrect number of arguments, use exactly 1 argument - either an integer or hostname/IP"
    end
    startsupervisor(opts)
  end

  defp parseoption(opts) do
    try do
      arg = opts |> hd |> String.to_integer
    rescue 
      _ -> arg = opts |> hd #just return the first element as is
    end
  end

  def startsupervisor(opts) do
    arg = parseoption(opts)
    if is_integer(arg) do
      mode = :local
      listenerpid = spawn(BitcoinListener, :loop, [])
      Process.register(listenerpid, :listenerpid)
      registerpid = spawn(BitcoinListener, :clientregister,[arg])
      Process.register(registerpid, :registerpid)
      masterserver = listenerpid
      masternode = "master@" <> getipaddress() |> String.to_atom
      Node.start(masternode)
      Node.set_cookie(:"BitcoinMiner")
    else
      mode = :remote
      masternode = "master@" <> arg |> String.to_atom
      clientname = "client" <> to_string(:rand.uniform(100)) <> "@" <> getipaddress() |> String.to_atom
      Node.start(clientname)
      Node.set_cookie(:"BitcoinMiner")
      Node.connect(masternode)
      selfpid = self();
      masterserver = masternode
      Node.spawn(masternode, fn -> send(:registerpid, {:register, selfpid}) end)
      receive do
        numberofzeroes -> arg = numberofzeroes
      end
    end
    #start a number of workers here
    randlength = 15; #length of the random string
    numberofcores = System.schedulers_online
    startworkerpercore(numberofcores,0, arg, randlength, masterserver, mode, opts)
    receive do
    _ -> IO.puts "Never will happen" #Only to keep the thread alive so that lines can be printed to stdout
    end
  end

  def startworkerpercore(numberofcores, currentspawned, arg, randlength, masterserver, mode, opts) do
    if currentspawned < numberofcores do
      pid = spawn(BitcoinProject1, :spinupworker, [arg, randlength, masterserver, mode, opts])
      startworkerpercore(numberofcores, (currentspawned + 1), arg, randlength, masterserver, mode, opts)
    end
  end

  def spinupworker(arg, randlength, masterserver, mode, opts) do
    pid = spawn(BitcoinProject1, :dostartworkerloop, [arg, randlength, masterserver, mode, opts])
    ref = Process.monitor(pid)
    receive do
      {:DOWN, ^ref, _, _, _} -> spinupworker(arg, randlength, masterserver, mode, opts)
    end
  end

  def dostartworkerloop(arg, randlength, masterserver, mode, opts) do
    pid = spawn(HashWorker, :starthashactor, [arg, randlength, masterserver, mode])
    ref = Process.monitor(pid)
    receive do
      {:DOWN, ^ref, _, _, _} -> dostartworkerloop(arg, randlength, masterserver, mode, opts)
    end
  end

  defp getipaddress do
    {:ok, iplist} = :inet.getif
    iplist |> hd |> elem(0) |> :inet.ntoa |> to_string
  end
end
