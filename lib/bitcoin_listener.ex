defmodule BitcoinListener do
    def loop do
        receive do
            {string, hash} -> IO.puts (string <> "\t" <> hash)
        end
        loop()
    end

    def clientregister (numberzeroes) do
        receive do
            {:register, pid} -> send pid, numberzeroes
        end
        clientregister(numberzeroes)
    end
end