# COP5615 - Project 1

## Building the code

Use `mix escript.build` to generate a local escript binary called `project1`

### Note about running the code
To run this application EPMD must be running as a daemon, run `epmd -daemon` before starting the application to avoid errors like:
```
** (FunctionClauseError) no function clause matching in :erlang.set_cookie/2
```
## Discussion

1. **Work unit size**: Each worker performs ```10^(1 + number_of_zeroes)``` computations per lifetime before restarting and attempting a different string. Since all the workers do independent work (ideally, since their input prefix strings are randomly chosen), and SHA-256 is considered to be evenly distributed, making each worker go through an exponential function of the difficulty (k) lines up with expected values along the distribution.

2. Output of `./project1 4`:
```
$ ./project1 4
kraghunathanloi739G/hQUiuvUAMD563967    0000E9D4B1CA5E402477A4CB3BA63CFCEEB157580AF203E1D43F5398EB72D53E
kraghunathanVNt3kbzuQcquQrGRqT4K25752   0000AA148FF77318438A6791DEC62FD8398A674116D06478D03BADC96DC6BC69
kraghunathanunLqUqoK3bWKVCnbQJgW62497   00005748B6EDD4347B61F8B493893DAF6E01866826CB36D467F3675597FBB957
kraghunathanloi739G/hQUiuvUAMD5677342   0000902CAFE98234F103302FD9517D2B2523FC654605C9E525401383B0CB3A41
kraghunathanunLqUqoK3bWKVCnbQJgW79163   0000A0E7D9F76568AEA56D0330F0FF8E29AA6DB3F5A76C55D231E043BEB0DD41
kraghunathan5evfdtWm4Sckozei9H7m87307   0000D63A92C84B2A4681129F744B9967A39F854E1C09096CFA975301279EDA30
kraghunathanEQkRK75hmJ4vgHA/7P+h32709   0000C9A2CB9EA30AF62C146E02432DAD1A4FDB2A104A6715D651006FF03116D6
kraghunathani2j2MOVXIkJLupYOyAro39791   000012F2495746706D1D78A71B98BEF1D95445E8584BDD79FBEA6C9B64777467
kraghunathan8JKL7GU2TzZoGqR2vORK56172   0000CAE48CC8FA892A565C6A602D75F900BDF55B0E8DC22156FE151F7399549F
kraghunathan8JKL7GU2TzZoGqR2vORK75750   0000903B335B7793209331646B07BE8ADAC28788304382016659B29D6ABAA008
kraghunathan8JKL7GU2TzZoGqR2vORK82936   0000B2F1056D11807EC60CD823B542CCC408E595696EC93AC6749761C1FD3BC5
kraghunathan8JKL7GU2TzZoGqR2vORK88296   0000FD8F1A32616E44F6B06A35F602C3DDE85ADB10DBDE119C0BD312554A4E3D
kraghunathanf0vEuT079acxDLu1JbuU92018   000025F9109F6BA622F1D8A4BCAD323A3CB4C1CD7C1D91AED496B892281209A1
```

3. Running time of `./project1 4` with CPU and Real time ratios:
I ran the binary for exactly 60s (using the `timeout` command) to get the following results:

```
$ time timeout -sHUP 60s ./project1 4 > /dev/null

real    1m.009s
user    6m50.359s
sys     0m48.375s
```
  * The ratio of CPU time to REAL time is: `410 / 60 ~ 6.83`. This was achieved on a computer with multiple users running and I believe I will get better results standalone

5. Coin with most zeros:
    - 7 leading zeros: kraghunathandlwV53znzxs/qUW/xlXQ11293922
6. Largest number of working machines in the network:
    - 2 machines

## Decisions
The SHA hash is assumed to be evenly distributed. The chance of finding a coin with zeroes as expected is equally likely with close strings as with faraway strings (edit distance), therefore there seemed to be no real requirement to "distribute" an enumeration of strings amongst multiple workers, only to make sure that workers were not hashing the same input. With perfect random strings the chances of collision increase as per the Birthday theorem, so to avoid too much similarity the workers were implemented to hash a random string prefix with a counter. Fault tolerance is pretty good, since all processes are mostly independent - any found strings are sent asynchronously to a listener on the server which outputs to IO.